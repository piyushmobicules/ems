var dao = require("../dao");
var model = require("../model");

var createEmployee = function (emp, callback) {

    var empjson = new model.db.Employee(emp);
    dao.employeeDao.basicOperations.insert(empjson, function (resp) {
        callback(empjson);
    });
};


module.exports = {

    addNewEmployee : createEmployee
}