var dao = require("../dao");
var model = require("../model");

var createCompany = function (comp, callback) {
	var compjson = new model.db.Company(comp);
	dao.companyDao.basicOperations.insert(compjson, function (resp) {
		callback();
	});
};

module.exports = {

	addNewCompany :createCompany


}