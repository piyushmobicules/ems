var dao = require("../dao");
var model = require("../model");

var createProject = function (project, callback) {
  var creator_id = project.creator_id;

    dao.projectDao.getCreatorComp(creator_id, function (err, employee){

        project.creator_comp_id = employee.emp_employer_id;
        var projectjson = new model.db.Project(project);
        dao.projectDao.basicOperations.insert(projectjson, function (err, resp) {
            callback(err,resp);
         });
    });
};

var allProjects = function(callback){
    dao.projectDao.findAll(callback);

};

var getProjectByComp = function (id , callback){
    dao.projectDao.getProjectByComp(id, function(res){
        if(res)
            callback();

    });

};


module.exports = {

    addNewProject : createProject,
    allProjects :allProjects,
    getProjectByComp :getProjectByComp

}