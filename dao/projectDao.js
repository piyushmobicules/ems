var baseDao = require("./baseDao");
var projectColl = baseDao.getCollectionName('project');
var employeeColl = baseDao.getCollectionName('employee');
var fieldsToShow = { name : true, client : true, };


var findAll = function (callback){
    projectColl.find( {},fieldsToShow).toArray(callback);
};

var getProjectByComp  = function(id,callback){
    var query = { _id : baseDao.getId(id)};
    projectColl.find(query,fieldsToShow).toArray(callback);
};

var getCreatorComp = function (id, callback){
    var query = {_id: baseDao.getId(id)};
    var toShow = { _id: false, emp_employer_id:true};
    employeeColl.findOne( query, toShow, callback);
};


module.exports = {
    basicOperations : new baseDao.basicOperations('project'),
    findAll : findAll,
    getProjectByComp :getProjectByComp,
    getCreatorComp : getCreatorComp

}