
module.exports ={
    companyDao : require('./companyDao'),
    employeeDao : require('./employeeDao'),
    projectDao : require('./projectDao'),
}
