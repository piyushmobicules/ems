var ObjectID = require('mongodb').ObjectID;
var dbFactory = require('./daoConfig');

function getCollectionName(name){
    return dbFactory.getdb().collection(name);
}


var basicOperations = function(name){

    this.insert = function(json, callback){
        getCollectionName(name).insert(json,callback);
    };

    this.remove = function(_id, callback){
        getCollectionName(name).remove({_id:getId(_id)},callabck);
    };

    this.update = function (_id, json, callback){
        getCollectionName(name).update({_id:getId(_id)}, json, callback);
    };

    this.find = function (_id, callback){
        getCollectionName(name).findOne({_id: getId(_id)}, callback);
    };


};



var getId = function(_id){
    if(_id._bsontype && _id._bsontype==='ObjectID'){
        return _id;
    }else{
        return new ObjectID(_id);
    }
};


module.exports ={

    getCollectionName:getCollectionName,
    getId : getId,
    basicOperations : basicOperations
}

