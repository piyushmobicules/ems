var MongoClient = require('mongodb').MongoClient;
var db;

exports.getdb = function(){
	return db;
};

exports.init = function(dbname, callback){
	// Initialize connection once
	MongoClient.connect("mongodb://localhost:27017/"+dbname, function(err, database) {
	  db = database;
	  console.log("connected to db " + dbname);
	  if(err) throw err;
	  callback();
	});
};


