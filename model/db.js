var baseDao = require("../dao/baseDao");


function Company(comp){

	this.name = comp.name;
	this.email = comp.email;
	this.address = comp.address;
    this.city = comp.city;
	this.contact = comp.contact;
	this.desc = comp.desc;
	var dd =  comp.estab;
	this.estab = new Date(dd).getTime();
	removeNullKeys(this);
};


function Employee(emp) {
	this.emp_name  = emp.emp_name;
	this.emp_email = emp.emp_email ;
	var date1 = emp.emp_doj;
	this.emp_doj = new Date(date1).getTime();
	this.emp_designation = emp.emp_designation;
	this.emp_typeofemp = emp.emp_typeofemp;
	var date2 = emp.emp_dob;
	this.emp_dob = new Date(date2).getTime();
	this.emp_employer_id = baseDao.getId(emp.emp_employer_id);
	removeNullKeys(this);
}


function Project(project) {

	this.name  = project.name;
	this.client = project.client;
	this.others = project.others;
	var start1 = project.start;
	this.start = parseInt(new Date(start1).getTime());
	var end1 = project.end;
	this.end = parseInt(new Date(end1).getTime());
	this.creator_id = project.creator_id;
	this.creator_comp_id = project.creator_comp_id;
	removeNullKeys(this);
}





module.exports = {

    Company : Company,
	Employee : Employee,
	Project : Project,


};

function removeNullKeys(ob){
	for(var i in ob){
		if(ob[i]==null){
			delete ob[i];
		}
	}
};
