var app = require("../app");
var service = require("../service");


app.post('/add/project', function(req,res){

var project ={};
    project.name = req.body.name;
    project.client = req.body.client;
    project.others = req.body.others;
    project.start =  req.body.start;
    project.end = req.body.end;
    project.creator_id = req.body.creator_id;

    service.projectService.addNewProject(project, function(err, projectjson){
        res.send(projectjson);
    });
});

app.post('/all/projects', function(req, res){
    service.projectService.allProjects( function(err, result){
        if(err)
            res.send('Something went wrong!!');
        else
            res.send(result);
    });
});

app.post('/projects/byCompany/:id', function(req, res){
    var id = req.params.id;
    console.log('compnay id: ' + id);
     service.projectService.getProjectByComp( id, function(err, result){
         if(err)
             res.send('Something went wrong!!');
         else
             res.send(result);
     });

});




