var app = require('../app');
var service = require('../service');

/* GET users listing. */
app.get('/', function(req, res) {

    res.send('Index page of Ems');


});

app.post('/add/company', function(req, res) {

    var comp={};
    comp.name = req.body.name;
    comp.email = req.body.email;
    comp.address = req.body.address;
    comp.city =  req.body.city;
    comp.contact = req.body.contact;
    comp.desc = req.body.desc;
    comp.estab = req.body.estab;

    service.companyService.addNewCompany(comp,function(){
        res.send(JSON.stringify(comp));
    });



});
